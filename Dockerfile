ARG DOCKER_REGISTRY=registry.redhat.io
FROM ${DOCKER_REGISTRY}/openjdk/openjdk-11-rhel7
COPY target/*.jar /deployments/userunit.jar
ENV TZ="Europe/Moscow"
#LABEL version="look at pom.xml"
EXPOSE 8080/tcp
WORKDIR /deployments
CMD ["java", "-jar", "userunit.jar"]