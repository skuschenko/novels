package ru.novel.model;

import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.Table;
import java.io.Serializable;
import java.util.Objects;
import java.util.Set;

import static ru.novel.constant.Constant.GENRES_TABLE;

@Data
@Table(name = GENRES_TABLE)
@Entity
@NoArgsConstructor
public class Genre extends AbstractTranslate implements Serializable {
    public Genre(String englishName) {
        super(englishName);
    }

    @ManyToMany
    @JoinTable(name = "novel_genre",
            joinColumns = @JoinColumn(name = "genre_id"),
            inverseJoinColumns = @JoinColumn(name = "novel_id"))
    private Set<Novel> novels;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Genre)) return false;
        final Genre genre = (Genre) o;
        return Objects.equals(this.englishName, genre.getEnglishName());
    }

    @Override
    public int hashCode() {
        return getClass().hashCode();
    }

}
