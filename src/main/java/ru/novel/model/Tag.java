package ru.novel.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.Table;
import java.io.Serializable;
import java.util.Objects;
import java.util.Set;

import static ru.novel.constant.Constant.TAGS_TABLE;

@Data
@Table(name = TAGS_TABLE)
@Entity
@AllArgsConstructor
@NoArgsConstructor
public class Tag extends AbstractTranslate implements Serializable {

    public Tag(String englishName) {
        super(englishName);
    }

    @ManyToMany
    @JoinTable(name = "novel_tag",
            joinColumns = @JoinColumn(name = "tag_id"),
            inverseJoinColumns = @JoinColumn(name = "novel_id"))
    private Set<Novel> novels;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Tag)) return false;
        final Tag tag = (Tag) o;
        return Objects.equals(this.englishName, tag.getEnglishName());
    }

    @Override
    public int hashCode() {
        return getClass().hashCode();
    }

}
