package ru.novel.model;

import lombok.Data;

import javax.persistence.Entity;
import javax.persistence.Table;
import java.io.Serializable;

import static ru.novel.constant.Constant.NOVEL_CHAPTER_TEXT_TABLE;

@Data
@Table(name = NOVEL_CHAPTER_TEXT_TABLE)
@Entity
public class NovelChapterText extends AbstractTranslate implements Serializable {

}
