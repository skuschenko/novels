package ru.novel.model;

import lombok.AllArgsConstructor;
import lombok.Data;

import javax.persistence.Entity;
import javax.persistence.Table;
import java.io.Serializable;

import static ru.novel.constant.Constant.LANGUAGE_TABLE;

@Data
@Table(name = LANGUAGE_TABLE)
@Entity
@AllArgsConstructor
public class Language extends AbstractTranslate implements Serializable {
    public Language(String englishName) {
        super(englishName);
    }
}
