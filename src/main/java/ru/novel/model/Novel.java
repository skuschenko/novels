package ru.novel.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;
import java.util.UUID;

import static ru.novel.constant.Constant.GENRE_ID;
import static ru.novel.constant.Constant.NOVEL_GENRE_TABLE;
import static ru.novel.constant.Constant.NOVEL_ID;
import static ru.novel.constant.Constant.NOVEL_TABLE;
import static ru.novel.constant.Constant.NOVEL_TAG_TABLE;
import static ru.novel.constant.Constant.TAG_ID;


@Data
@Builder
@Entity(name = NOVEL_TABLE)
@AllArgsConstructor
@NoArgsConstructor
public class Novel implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private UUID id;

    @ManyToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "status_title_id", referencedColumnName = "id")
    private StatusTitle statusTitle;

    @ManyToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "status_translation_id", referencedColumnName = "id")
    private StatusTranslation statusTranslation;

    @Column(name = "chapter")
    private String chapter;

    @OneToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "name_id", referencedColumnName = "id")
    private Name name;

    @Column(name = "alternate")
    private String alternate;

    @Column(name = "year")
    private String yearOfPublishing;

    @Column(name = "link")
    private String link;

    @Column(name = "link_img")
    private String linkImg;

    @ManyToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "status_language_id", referencedColumnName = "id")
    private Language language;

    @OneToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "description_id", referencedColumnName = "id")
    private Description description;

    @OneToMany(mappedBy = "novel",cascade = CascadeType.PERSIST)
    private Set<NovelChapter> novelChapters = new HashSet<>();

    @ManyToMany(cascade = CascadeType.ALL)
    @JoinTable(name = NOVEL_TAG_TABLE,
            joinColumns = @JoinColumn(name = NOVEL_ID),
            inverseJoinColumns = @JoinColumn(name = TAG_ID))
    private Set<Tag> tags = new HashSet<>();

    @ManyToMany(cascade = CascadeType.ALL)
    @JoinTable(name = NOVEL_GENRE_TABLE,
            joinColumns = @JoinColumn(name = NOVEL_ID),
            inverseJoinColumns = @JoinColumn(name = GENRE_ID))
    private Set<Genre> genres = new HashSet<>();

    @Column(name = "author")
    private String author;

    @Column(name = "parsed_flg")
    private Boolean parsedFlg = false;


}
