package ru.novel.model;

import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Entity;
import javax.persistence.Table;
import java.io.Serializable;

import static ru.novel.constant.Constant.DESCRIPTION_TABLE;

@Data
@Table(name = DESCRIPTION_TABLE)
@Entity
@NoArgsConstructor
public class Description extends AbstractTranslate implements Serializable {
    public Description(String englishName) {
        super(englishName);
    }
}
