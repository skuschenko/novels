package ru.novel.model;

import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Entity;
import javax.persistence.Table;
import java.io.Serializable;

import static ru.novel.constant.Constant.NAME_TABLE;

@Data
@Table(name = NAME_TABLE)
@Entity
@NoArgsConstructor
public class Name extends AbstractTranslate implements Serializable {
    public Name(String englishName) {
        super(englishName);
    }
}
