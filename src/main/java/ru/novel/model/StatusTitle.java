package ru.novel.model;

import lombok.AllArgsConstructor;
import lombok.Data;

import javax.persistence.Entity;
import javax.persistence.Table;
import java.io.Serializable;

import static ru.novel.constant.Constant.STATUS_TITLE_TABLE;

@Data
@Table(name = STATUS_TITLE_TABLE)
@Entity
@AllArgsConstructor
public class StatusTitle extends AbstractTranslate implements Serializable {
    public StatusTitle(String englishName) {
        super(englishName);
    }
}
