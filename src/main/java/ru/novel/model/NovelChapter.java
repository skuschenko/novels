package ru.novel.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import java.io.Serializable;

import static ru.novel.constant.Constant.NOVEL_CHAPTER_TABLE;

@Data
@Table(name = NOVEL_CHAPTER_TABLE)
@Entity
@Builder
@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode(onlyExplicitlyIncluded = true)
public class NovelChapter extends AbstractTranslate implements Serializable {

    @ManyToOne
    @JoinColumn(name="novel_id")
    private Novel novel;

    @EqualsAndHashCode.Include
    @Column(name = "link")
    private String link;

    @Column(name = "translated_flg")
    private Boolean translatedFlg = false;

    @OneToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "novel_chapter_text_id", referencedColumnName = "id")
    private NovelChapterText novelChapterText;

}
