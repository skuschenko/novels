package ru.novel.model;

import lombok.AllArgsConstructor;
import lombok.Data;

import javax.persistence.Entity;
import javax.persistence.Table;
import java.io.Serializable;

import static ru.novel.constant.Constant.STATUS_TRANSLATION_TABLE;

@Data
@Table(name = STATUS_TRANSLATION_TABLE)
@Entity
@AllArgsConstructor
public class StatusTranslation extends AbstractTranslate implements Serializable {
    public StatusTranslation(String englishName) {
        super(englishName);
    }
}
