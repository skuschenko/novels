package ru.novel.constant;

public interface Constant {
    String STATUS_TITLE_TABLE = "status_title";
    String STATUS_TRANSLATION_TABLE = "status_translation";
    String NOVEL_TABLE = "novel";
    String LANGUAGE_TABLE = "language";
    String NOVEL_CHAPTER_TABLE = "novel_chapter";
    String DESCRIPTION_TABLE = "description";
    String NAME_TABLE = "name";
    String NOVEL_CHAPTER_TEXT_TABLE = "novel_chapter_text";
    String TAGS_TABLE = "tag";
    String GENRES_TABLE = "genre";
    String NOVEL_TAG_TABLE = "novel_tag";
    String NOVEL_GENRE_TABLE = "novel_genre";

    String NOVEL_ID = "novel_id";
    String TAG_ID = "tag_id";
    String GENRE_ID = "genre_id";

}
