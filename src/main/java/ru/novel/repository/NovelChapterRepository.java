package ru.novel.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.novel.model.NovelChapter;

import java.util.UUID;

public interface NovelChapterRepository extends JpaRepository<NovelChapter, UUID> {

}
