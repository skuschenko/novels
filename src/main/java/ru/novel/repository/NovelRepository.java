package ru.novel.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import ru.novel.model.Novel;

import java.util.Optional;
import java.util.UUID;

public interface NovelRepository extends JpaRepository<Novel, UUID> {

    Optional<Novel> findNovelByLinkAndParsedFlgIsFalse(String link);

    @Query(nativeQuery = true, value = "select * from novel where description_id is null limit 1")
    Optional<Novel> getFirstNovel();

}
