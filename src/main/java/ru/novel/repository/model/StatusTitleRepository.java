package ru.novel.repository.model;

import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import ru.novel.model.Language;
import ru.novel.model.StatusTitle;

import java.util.Optional;

@Repository
public interface StatusTitleRepository extends AbstractTranslateRepository<StatusTitle> {

    @Query( "select e from StatusTitle e where e.englishName =:englishName")
    Optional<StatusTitle> getRecord(String englishName);

}
