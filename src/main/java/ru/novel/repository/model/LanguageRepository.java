package ru.novel.repository.model;

import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import ru.novel.model.Language;

import java.util.Optional;

@Repository
public interface LanguageRepository extends AbstractTranslateRepository<Language> {

    @Query( "select e from Language e where e.englishName =:englishName")
    Optional<Language> getRecord(String englishName);

}
