package ru.novel.repository.model;

import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import ru.novel.model.Genre;

import java.util.Optional;

@Repository
public interface GenreRepository extends AbstractTranslateRepository<Genre> {

    @Query( "select e from Genre e where e.englishName =:englishName")
    Optional<Genre> getRecord(String englishName);

}
