package ru.novel.repository.model;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.NoRepositoryBean;
import ru.novel.model.AbstractTranslate;

import java.util.Optional;
import java.util.UUID;

@NoRepositoryBean
public interface AbstractTranslateRepository<E extends AbstractTranslate> extends JpaRepository<E, UUID> {

    Optional<E> getRecord(String englishName);

}
