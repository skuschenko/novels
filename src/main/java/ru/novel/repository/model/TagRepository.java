package ru.novel.repository.model;

import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import ru.novel.model.Tag;

import java.util.Optional;

@Repository
public interface TagRepository extends AbstractTranslateRepository<Tag> {

    @Query( "select e from Tag e where e.englishName =:englishName")
    Optional<Tag> getRecord(String englishName);

}
