package ru.novel.repository.model;

import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import ru.novel.model.StatusTranslation;

import java.util.Optional;

@Repository
public interface StatusTranslationRepository extends AbstractTranslateRepository<StatusTranslation> {

    @Query( "select e from StatusTranslation e where e.englishName =:englishName")
    Optional<StatusTranslation> getRecord(String englishName);

}
