package ru.novel.service;


import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.concurrent.atomic.AtomicBoolean;

@Service
public class ElementService {

    private WebDriver driver;

    public ElementService(WebDriver driver) {
        this.driver = driver;
    }

    public boolean isNextPage() {
        final AtomicBoolean isNextPage = new AtomicBoolean(false);
        final WebElement element = findElementByClass("page_next");
        Optional.ofNullable(element).ifPresent(el -> {
            final WebElement elementA = findElementByTag(el, "a");
            Optional.ofNullable(elementA).ifPresent(a -> {
                isNextPage.set(true);
                navigate(getHrefFromA(a));
            });
        });
        return isNextPage.get();
    }

    public WebElement findElementByClass(String className) {
        try {
            return driver.findElement(By.className(className));
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    public WebElement findElementByXpath(String xpath) {
        return driver.findElement(By.xpath(xpath));
    }

    public WebElement findImgByClassName(String className) {
        try {
            return findElementByXpath("//div[@class='" + className + "']/a/img");
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    public String getImgSrcByClassName(String className) {
        return findImgByClassName(className).getAttribute("src");
    }

    public WebElement findElementByTag(String tagName) {
        return driver.findElement(By.tagName(tagName));
    }

    public WebElement findElementById(String id) {
        try {
            return driver.findElement(By.id(id));
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    public WebElement findElementById(WebElement element, String id) {
        return element.findElement(By.className(id));
    }

    public WebElement findElementByClass(WebElement element, String className) {
        try {
            return element.findElement(By.className(className));
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    public List<WebElement> findLiElementsByClass(String className) {
        try {
            return findElementByClass(className).findElements(By.tagName("li"));
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    public List<WebElement> findAElementsById(String id) {
        try {
            return findElementById(id).findElements(By.tagName("a"));
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    public WebElement findElementByTag(WebElement element, String tagName) {
        try {
            return element.findElement(By.tagName(tagName));
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    public String getTextFromLi(List<WebElement> li, String name) {
        return li.stream()
                .filter(item -> item.getText().contains(name))
                .findFirst()
                .map(webElement -> {
                    var el = findElementByTag(webElement, "span");
                    if (Optional.ofNullable(el).isPresent()) {
                        return el.getText();
                    }
                    el = findElementByTag(webElement, "a");
                    if (Optional.ofNullable(el).isPresent()) {
                        return el.getText();
                    }
                    return null;
                }).orElse(null);
    }

    public WebElement findElementA(WebElement element) {
        try {
            return element.findElement(By.tagName("a"));
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    public void navigate(String url) {
        driver.navigate().to(url);
        final WebElement element = findElementById("content");
        Optional.ofNullable(element).flatMap(e -> Optional.ofNullable(findElementByTag(e, "div")))
                .ifPresent(WebElement::click);
        waiting();
    }

    public void navigateJS(String url) {
        final JavascriptExecutor js = (JavascriptExecutor) driver;
        js.executeScript("var q = document.getElementsByTagName('body')[0];" +
                "q.innerHTML ='<a href=\"" + url + "\" id=\"qqq\">...</a>';" +
                "document.getElementById('qqq').click()");
        waiting();
    }

    public void setDisplayBlock(String className) {
        final WebElement element = findElementByClass(className);
        final JavascriptExecutor js = (JavascriptExecutor) driver;
        js.executeScript("arguments[0].style.display='block'", element);
    }

    public List<WebElement> findListElementsByClass(String className) {
        return driver.findElements(By.className(className));
    }

    public List<WebElement> findListElementsByClass(WebElement element, String className) {
        return element.findElements(By.className(className));
    }

    public List<WebElement> findListElementsByTag(WebElement element, String tagName) {
        return element.findElements(By.tagName(tagName));
    }

    public String getHrefFromA(WebElement element) {
        if (!"a".equalsIgnoreCase(element.getTagName())) {
            element = findElementA(element);
            if (Optional.ofNullable(element).isEmpty()) {
                return null;
            }
        }
        return element.getAttribute("href");
    }

    public void waiting() {
        new WebDriverWait(driver, 20).until(
                webDriver -> ((JavascriptExecutor) webDriver).executeScript("return document.readyState").equals("complete"));
        try {
            Thread.sleep(2000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    public void waiting(int seconds) {
        new WebDriverWait(driver, 20).until(
                webDriver -> ((JavascriptExecutor) webDriver).executeScript("return document.readyState").equals("complete"));
        try {
            Thread.sleep(seconds * 1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

}
