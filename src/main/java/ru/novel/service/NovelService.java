package ru.novel.service;

import org.openqa.selenium.WebElement;
import org.springframework.stereotype.Service;
import ru.novel.model.Description;
import ru.novel.model.Genre;
import ru.novel.model.Language;
import ru.novel.model.Name;
import ru.novel.model.Novel;
import ru.novel.model.NovelChapter;
import ru.novel.model.StatusTitle;
import ru.novel.model.StatusTranslation;
import ru.novel.model.Tag;
import ru.novel.repository.NovelChapterRepository;
import ru.novel.repository.NovelRepository;
import ru.novel.repository.model.GenreRepository;
import ru.novel.repository.model.LanguageRepository;
import ru.novel.repository.model.StatusTitleRepository;
import ru.novel.repository.model.StatusTranslationRepository;
import ru.novel.repository.model.TagRepository;
import ru.novel.service.api.INovelService;

import java.util.HashSet;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;


@Service
public class NovelService implements INovelService {

    private final static String LIST_URLS =
            "https://ranobes.net/novels1/f/status-end=Completed/status-trs=Completed/sort=date/order=desc/";

    private final NovelRepository novelRepository;
    private final TagRepository tagRepository;
    private final GenreRepository genreRepository;
    private final LanguageRepository languageRepository;
    private final StatusTitleRepository statusTitleRepository;
    private final StatusTranslationRepository statusTranslationRepository;
    private final NovelChapterRepository novelChapterRepository;

    private final ElementService elementService;


    public NovelService(NovelRepository novelRepository, ElementService elementService, TagRepository tagRepository,
                        GenreRepository genreRepository, LanguageRepository languageRepository,
                        StatusTitleRepository statusTitleRepository, StatusTranslationRepository statusTranslationRepository,
                        NovelChapterRepository novelChapterRepository) {
        this.novelRepository = novelRepository;
        this.elementService = elementService;
        this.tagRepository = tagRepository;
        this.genreRepository = genreRepository;
        this.languageRepository = languageRepository;
        this.statusTitleRepository = statusTitleRepository;
        this.statusTranslationRepository = statusTranslationRepository;
        this.novelChapterRepository = novelChapterRepository;
    }


    @Override
    public void getNovels() {
        elementService.navigate(LIST_URLS);
        do {
            final WebElement content = elementService.findElementById("dle-content");
            final List<WebElement> webElements = elementService.findListElementsByClass(content, "title");
            webElements.forEach(element -> {
                final Novel novel = Novel.builder().
                        link(elementService.getHrefFromA(element))
                        .parsedFlg(false)
                        .build();
                if (novelRepository.findNovelByLinkAndParsedFlgIsFalse(novel.getLink()).isEmpty()) {
                    novelRepository.save(novel);
                }
            });
        }
        while (elementService.isNextPage());
    }

    @Override
    public void getNovelsInfo() {
        while(novelRepository.getFirstNovel().isPresent()){
            getNovelInfo();
            elementService.waiting();
        }
    }

    @Override
    public void getNovelInfo() {
        elementService.navigate(LIST_URLS);
        final Novel novel = novelRepository.getFirstNovel().orElseThrow(() -> new RuntimeException("Error while get first novel"));
        elementService.navigateJS(novel.getLink());
        final WebElement tagsBtn = elementService.findElementByClass(elementService.findElementById("mc-fs-keyw"), "showcont-btn");
        Optional.ofNullable(tagsBtn).ifPresent(t->{
            if (t.isDisplayed()) {
                t.click();
            }
            final Set<Tag> tags = elementService.findAElementsById("mc-fs-keyw").stream()
                    .map(item -> getTag(item.getText())).collect(Collectors.toSet());
            novel.setTags(tags);
        });
        final Set<Genre> genres = elementService.findAElementsById("mc-fs-genre").stream()
                .map(item -> getGenre(item.getText())).collect(Collectors.toSet());
        final List<WebElement> li = elementService.findLiElementsByClass("r-fullstory-spec");
        novel.setAlternate(elementService.findElementByClass("subtitle").getText());
        novel.setLinkImg(elementService.getImgSrcByClassName("poster"));
        novel.setParsedFlg(false);
        novel.setYearOfPublishing(elementService.getTextFromLi(li, "Year of publishing"));
        novel.setAuthor(elementService.getTextFromLi(li, "Authors"));
        novel.setName(new Name(elementService.findElementByClass("title").getText().split("\n")[0]));
        novel.setChapter(elementService.getTextFromLi(li, "In original") == null ?
                elementService.getTextFromLi(li, "Available").split(" ")[0] :
                elementService.getTextFromLi(li, "In original"));
        elementService.setDisplayBlock("moreless__full");
        final Description description = new Description(elementService.findElementByClass("moreless__full").getText());
        novel.setGenres(genres);
        novel.setLanguage(getLanguage(elementService.getTextFromLi(li, "Language")));
        novel.setStatusTranslation(getStatusTranslation(elementService.getTextFromLi(li, "Translation")));
        novel.setStatusTitle(getStatusTitle(elementService.getTextFromLi(li, "Status in COO")));
        novelRepository.save(novel);
        saveChapters(novel);
        novel.setDescription(description);
        novelRepository.save(novel);
    }

    private void saveChapters(Novel novel) {
        final Set<NovelChapter> novelChapters = getChapters().stream()
                .peek(novelChapter -> novelChapter.setNovel(novel))
                .collect(Collectors.toSet());
        novelChapterRepository.saveAll(novelChapters);
    }

    private Set<NovelChapter> getChapters() {
        final Set<NovelChapter> novelChapters = new HashSet<>();
        final WebElement chapterLink = elementService.findElementByClass("r-fullstory-chapters-foot");
        elementService.findListElementsByTag(chapterLink, "a").get(1).click();
        elementService.waiting();
        do {
            final WebElement content = elementService.findElementByClass("chapters__container");
            final List<WebElement> webElements = elementService.findListElementsByClass(content, "cat_block");
            webElements.remove(0);
            webElements.forEach(element -> {
                final NovelChapter novelChapter = NovelChapter.builder().
                        link(elementService.getHrefFromA(element))
                        .translatedFlg(false)
                        .build();
                novelChapters.add(novelChapter);
            });
        }
        while (elementService.isNextPage());
        return novelChapters;
    }

    private Genre getGenre(String name) {
        return genreRepository.getRecord(name).orElse(new Genre(name));
    }

    private Tag getTag(String name) {
        return tagRepository.getRecord(name).orElse(new Tag(name));
    }

    private Language getLanguage(String name) {
        return languageRepository.getRecord(name).orElse(new Language(name));
    }

    private StatusTranslation getStatusTranslation(String name) {
        return statusTranslationRepository.getRecord(name).orElse(new StatusTranslation(name));
    }

    private StatusTitle getStatusTitle(String name) {
        return statusTitleRepository.getRecord(name).orElse(new StatusTitle(name));
    }

}
