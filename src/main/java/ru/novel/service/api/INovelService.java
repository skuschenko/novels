package ru.novel.service.api;

public interface INovelService {

    void getNovels();
    void getNovelInfo();
    void getNovelsInfo();
}
