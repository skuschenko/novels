package ru.novel.util;


import lombok.experimental.UtilityClass;

import java.text.ParsePosition;
import java.text.SimpleDateFormat;
import java.time.LocalDate;

@UtilityClass
public class CheckObjectUtil {

    private static final String DATE_PATTERN_ISO = "yyyy-MM-dd";

    public static boolean isDateValid(String s) {
        final SimpleDateFormat sdf = new SimpleDateFormat(DATE_PATTERN_ISO);
        sdf.setLenient(false);
        return sdf.parse(s, new ParsePosition(0)) != null;
    }

    public static boolean compareDate(LocalDate start, LocalDate end) {
        return end.compareTo(start) >= 0;
    }

}
