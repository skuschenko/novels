package ru.novel;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.transaction.annotation.EnableTransactionManagement;

@SpringBootApplication
@EnableTransactionManagement
public class NovelApplication {

    public static void main(String[] args) {
         SpringApplication.run(NovelApplication.class, args);
    }

}
