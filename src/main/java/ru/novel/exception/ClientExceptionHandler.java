package ru.novel.exception;

import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import lombok.extern.slf4j.Slf4j;
import org.openqa.selenium.NoSuchElementException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

import java.util.Arrays;

@Slf4j
@ControllerAdvice
public class ClientExceptionHandler {

    @ExceptionHandler(value = RuntimeException.class)
    @ApiResponse(responseCode = "500",
            content = {@Content(mediaType = "application/json",
                    schema = @Schema(implementation = ErrorResponse.class))})
    public ResponseEntity<ErrorResponse> handle(RuntimeException exception) {
        Arrays.stream(exception.getStackTrace()).forEach(item -> log.error(item.toString()));
        return new ResponseEntity<>(new ErrorResponse(exception.getClass().getName(), exception.getLocalizedMessage()),
                HttpStatus.INTERNAL_SERVER_ERROR);
    }

    @ExceptionHandler(value = NoSuchElementException.class)
    @ApiResponse(responseCode = "500",
            content = {@Content(mediaType = "application/json",
                    schema = @Schema(implementation = ErrorResponse.class))})
    public ResponseEntity<ErrorResponse> handle(NoSuchElementException exception) {
        Arrays.stream(exception.getStackTrace()).forEach(item -> log.error(item.toString()));
        return new ResponseEntity<>(new ErrorResponse(exception.getClass().getName(), exception.getLocalizedMessage()),
                HttpStatus.INTERNAL_SERVER_ERROR);
    }

}
