package ru.novel.api;

import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;

@Tag(name = "Novel API")
@RequestMapping("/sites")
public interface SiteApi {

    ResponseEntity<Void> getNovels() ;

    ResponseEntity<Void> getNovelInfo() ;

    ResponseEntity<Void> getNovelsInfo() ;

}

