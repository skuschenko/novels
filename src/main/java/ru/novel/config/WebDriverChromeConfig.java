package ru.novel.config;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeDriverService;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.springframework.boot.autoconfigure.condition.ConditionalOnExpression;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import ru.novel.NovelApplication;

import javax.annotation.PreDestroy;
import java.io.File;
import java.net.URL;
import java.util.Collections;

@Configuration
@ConditionalOnExpression("'${driver}'.equalsIgnoreCase('chrome')")
public class WebDriverChromeConfig {

    private static final String CHROMEDRIVER_EXE = "chromedriver.exe";
    public WebDriver driver;

    public WebDriverChromeConfig() {
        final String driverFile = findFile();
        final DesiredCapabilities capabilities = DesiredCapabilities.chrome();
        final ChromeDriverService service = new ChromeDriverService.Builder()
                .usingDriverExecutable(new File(driverFile))
                .build();
        final ChromeOptions options = new ChromeOptions();

        options.addArguments("--no-sandbox"); // Bypass OS security model, MUST BE THE VERY FIRST OPTION
        // options.addArguments("--headless");
        options.setExperimentalOption("excludeSwitches", Collections.singletonList("enable-automation"));
        options.setExperimentalOption("useAutomationExtension", null);

        // Changing the user agent / browser fingerprint
        options.addArguments("window-size=1920,1080");
        options.addArguments("user-agent=Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/74.0.3729.169 Safari/537.36");
       // options.addArguments("--user-agent=Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/108.0.0.0 Safari/537.36");
        options.addArguments("--start-maximized"); // open Browser in maximized mode
        options.addArguments("--disable-infobars"); // disabling infobars
        options.addArguments("--disable-extensions"); // disabling extensions
        options.addArguments("--disable-gpu"); // applicable to windows os only
        options.addArguments("--disable-dev-shm-usage"); // overcome limited resource problems
        options.addArguments("--disable-blink-features=AutomationControlled");
        options.addArguments("--disable-browser-side-navigation");
        options.addArguments("--remote-debugging-port=9222");
        options.merge(capabilities);

        this.driver = new ChromeDriver(service, options);
           }

    private static String findFile() {
        final ClassLoader classLoader = NovelApplication.class.getClassLoader();
        final URL url = classLoader.getResource(CHROMEDRIVER_EXE);
        return url.getFile();
    }

    @Bean(name = "webDriver")
    public WebDriver webDriver(){
        return this.driver;
    }

    @PreDestroy
    private void quit(){
        this.driver.quit();
        this.driver.close();
    }

}
