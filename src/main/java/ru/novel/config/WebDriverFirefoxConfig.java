package ru.novel.config;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxOptions;
import org.springframework.boot.autoconfigure.condition.ConditionalOnExpression;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import ru.novel.NovelApplication;

import javax.annotation.PreDestroy;
import java.net.URL;

@Configuration
@ConditionalOnExpression("'${driver}'.equalsIgnoreCase('firefox')")
public class WebDriverFirefoxConfig {

    private static final String FIREFOXDRIVER_EXE = "geckodriver.exe";
    public WebDriver driver;

    public WebDriverFirefoxConfig() {
        final String driverFile = findFile();
//        GeckoDriverService driverService = new GeckoDriverService.Builder()
//                .usingDriverExecutable(new File(driverFile)).build();

       System.setProperty("webdriver.gecko.driver", driverFile);
        FirefoxOptions options = new FirefoxOptions();
     //   options.addArguments("--headless");
      /*  FirefoxBinary firefoxBinary = new FirefoxBinary(new File("C:\\Program Files\\Mozilla Firefox\\Новая папка\\firefox.exe"));
        options.setBinary(firefoxBinary);
        options.setCapability("marionette", true);*/
      //  this.driver = new FirefoxDriver(options);
        this.driver = new FirefoxDriver(options);
        driver.manage().window().maximize();
        driver.manage().deleteAllCookies();
//        driver.manage().timeouts().pageLoadTimeout(40, TimeUnit.SECONDS);
       // driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
    }

    private static String findFile() {
        final ClassLoader classLoader = NovelApplication.class.getClassLoader();
        final URL url = classLoader.getResource(FIREFOXDRIVER_EXE);
        return url.getFile();
    }

    @Bean(name = "webDriver")
    public WebDriver webDriver() {
        return this.driver;
    }

    @PreDestroy
    private void quit() {
        this.driver.quit();
        this.driver.close();
    }

}
