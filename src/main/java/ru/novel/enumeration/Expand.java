package ru.novel.enumeration;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum Expand {

    TREE_DIVISION("TreeDivision"),
    USER("Novel"),
    PERMISSIONS("Permissions"),
    ATTRIBUTES("Attributes"),
    DICTIONARIES("Dictionaries"),
    LOGINS("Logins"),
    ROLES("Roles");

    private String name;

}
