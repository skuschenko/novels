package ru.novel.controller;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import ru.novel.api.SiteApi;
import ru.novel.service.api.INovelService;

@Slf4j
@RestController
@RequestMapping("/ranobes")
public class RanobesController implements SiteApi {


    private final INovelService novelService;

    @Autowired
    public RanobesController(INovelService novelService) {
        this.novelService = novelService;
    }

    @GetMapping( value = "/novels"  )
    @Override
    public ResponseEntity<Void> getNovels() {
        novelService.getNovels();
        return new ResponseEntity<>(HttpStatus.OK);
    }

    @Override
    @GetMapping( value = "/novel-info"  )
    public ResponseEntity<Void> getNovelInfo() {
        novelService.getNovelInfo();
        return new ResponseEntity<>(HttpStatus.OK);
    }

    @Override
    @GetMapping( value = "/novels-info"  )
    public ResponseEntity<Void> getNovelsInfo() {
        novelService.getNovelsInfo();
        return new ResponseEntity<>(HttpStatus.OK);
    }


}
